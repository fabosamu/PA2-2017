#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <memory>
#include <algorithm>
using namespace std;
#endif /* __PROGTEST_ */
//==========================================================
class CThing
{
public:
	CThing( int weight )
	{}
	virtual ~CThing () {}
	virtual int getWeight () const
	{
		return 0;
	}
	virtual string getName () const
	{
		return "default";
	}
	virtual bool Dangerous () const
	{
		return true;
	}
private:
	string m_lelo;
};
//==========================================================
class CInsertable
{
public:
	CInsertable( void )
	{
	}
	virtual CInsertable & Add ( const CThing & src ) 
	{
		m_things . push_back( shared_ptr<CThing> ( new CThing(src) ) );
		m_stuff . push_back ( {src.getName(), src.getWeight()} );
		m_danger . push_back ( src.Dangerous() );
		return *this;
	}
	virtual void print ( ostream & os ) const
	{
		for ( int i = 0 ; i < (int)m_stuff.size() ; ++i )
		{
			if ( i != (int)m_stuff.size()-1 ){
				os << "+-";
				os << m_stuff[i] . first << "\n";
			} else {
				os << "\\-";
				os << m_stuff[i] . first << "\n";
			}
		}
	}
	virtual int WeightSum () const
	{
		int sumWeight = 0;
		for ( auto x : m_stuff ){
			sumWeight +=  x . second;
		}
		return sumWeight;
	}
	virtual int Count () const
	{
		return m_stuff.size();
	}
	virtual bool Danger () const
	{
		for ( auto x : m_danger ){
			if ( x ){
				return true;
			}
		}
		return false;
	}
	virtual bool IdenticalContents ( const CInsertable & src ) const
	{
		if ( src.m_stuff.size() != m_stuff.size() ) return false;
		vector<string> sorted1;
		for ( auto x : m_stuff ){
			sorted1.push_back(x.first);
		}
		vector<string> sorted2;
		for ( auto x : src . m_stuff ){
			sorted2 . push_back( x.first );
		}
		sort(sorted1.begin(), sorted1.end());
		sort(sorted2.begin(), sorted2.end());
		for ( int i = 0 ; i < (int)sorted1.size() ; ++i ){
// cout<<sorted1[i]<<endl;
// cout<<sorted2[i]<<endl;
			if (sorted1[i] != sorted2[i]) return false;
		}
		return true;
	}
private:
	vector<shared_ptr<CThing> > m_things;
	vector<pair<string, int>> m_stuff;
	vector<bool> m_danger;
};
//==========================================================
//==========================================================
class CKnife : public CThing
{
public:
	CKnife ( int bladeLength )
		: CThing ( 100 ),
		m_bladeLength(bladeLength)
	{
		m_name = "Knife, blade: "+to_string(bladeLength)+" cm";
	}
	virtual string getName() const
	{
		return m_name;
	}
	virtual int getWeight() const 
	{
		return 100;
	}
	virtual void print ( ostream & os ) const 
	{
		os << getName();
	}
	virtual bool Dangerous () const
	{
		return m_bladeLength > 7 ? true : false;
	}
private:
	int m_bladeLength;
	 string m_name;
};
//==========================================================
class CClothes : public CThing
{
public:
	CClothes ( string desc )
		: CThing ( 500 ),
		m_desc(desc)
	{
		m_name = "Clothes ("+desc+")";
	}
	virtual string getName() const
	{
		return m_name;
	}
	virtual int getWeight() const 
	{
		return 500;
	}
	virtual void print ( ostream & os ) const 
	{
		os << getName();
	}
	virtual bool Dangerous () const
	{
		return false;
	}
private:
	string m_desc;
	string m_name;
};
//==========================================================
class CShoes : public CThing
{
public:
	CShoes ()
		: CThing ( 750 )
	{
	}
	virtual string getName() const
	{
		return m_name;
	}
	virtual int getWeight() const 
	{
		return 750;
	}
	virtual void print ( ostream & os ) const 
	{
		os << getName();
	}
	virtual bool Dangerous () const
	{
		return false;
	}
private:
	 string m_name = "Shoes";
};
//==========================================================
class CMobile : public CThing
{
public:
	CMobile ( string manufacturer, string model )
		: CThing ( 150 ),
		m_manufacturer ( manufacturer ),
		m_model ( model )
	{
		m_name = "Mobile "+model+" by: "+manufacturer+"";
		if ( manufacturer == "Samsung" && model == "Galaxy Note S7" )
			m_isDangerous = true;
		else
			m_isDangerous = false;
	}
	virtual string getName() const
	{
		return m_name;
	}
	virtual int getWeight() const 
	{
		return 150;
	}
	virtual void print ( ostream & os ) const 
	{
		os << getName();
	}
	virtual bool Dangerous () const
	{
		// todo
		return m_isDangerous;
	}
private:
	string m_manufacturer, m_model;
	string m_name;
	bool m_isDangerous;
};
//==========================================================
class CSuitcase : public CInsertable
{
public:
	CSuitcase ( int w, int h, int d )
		: CInsertable(),
		m_w(w),
		m_h(h),
		m_d(d)
	{
	}
	virtual int Weight () const
	{
		return WeightSum()+2000;
	}
	// Count
	// Danger
	// IdenticalContents
	friend ostream & operator << ( ostream & os, const CSuitcase & x ) 
	{
		os << "Suitcase " << x.m_w <<"x"<< x.m_h <<"x"<< x.m_d << "\n";
		x . print( os );
		return os;
	}
private:
	int m_w;
	int m_h;
	int m_d;
};
//==========================================================
class CBackpack : public CInsertable
{
public:
	CBackpack ()
		: CInsertable()
	{
	}
	virtual int Weight () const
	{
		return WeightSum()+1000;
	}
	// Count
	// Danger
	// IdenticalContents
	friend ostream & operator << ( ostream & os, const CBackpack & x ) 
	{
		os << "Backpack\n";
		x . print ( os );
		return os;
	}
private:
	string m_jozef;
};
ostream & operator << ( std::ostream & os, const CInsertable & x );
//==========================================================
#ifndef __PROGTEST__
int main ( void )
{
	CSuitcase x ( 1, 2, 3 );
	CBackpack y;
	ostringstream os;
	x . Add ( CKnife ( 7 ) );
	x . Add ( CClothes ( "red T-shirt" ) );
	x . Add ( CClothes ( "black hat" ) );
	x . Add ( CShoes () );
	x . Add ( CClothes ( "green pants" ) );
	x . Add ( CClothes ( "blue jeans" ) );
	x . Add ( CMobile ( "Samsung", "J3" ) );
	x . Add ( CMobile ( "Tamtung", "Galaxy Note S7" ) );
// cout << x << endl;
// cout << "Suitcase 1x2x3\n"
// 			 "+-Knife, blade: 7 cm\n"
// 			 "+-Clothes (red T-shirt)\n"
// 			 "+-Clothes (black hat)\n"
// 			 "+-Shoes\n"
// 			 "+-Clothes (green pants)\n"
// 			 "+-Clothes (blue jeans)\n"
// 			 "+-Mobile J3 by: Samsung\n"
// 			 "\\-Mobile Galaxy Note S7 by: Tamtung\n" ;
	os . str ( "" );
	os << x;

	assert ( os . str () == "Suitcase 1x2x3\n"
			 "+-Knife, blade: 7 cm\n"
			 "+-Clothes (red T-shirt)\n"
			 "+-Clothes (black hat)\n"
			 "+-Shoes\n"
			 "+-Clothes (green pants)\n"
			 "+-Clothes (blue jeans)\n"
			 "+-Mobile J3 by: Samsung\n"
			 "\\-Mobile Galaxy Note S7 by: Tamtung\n" );
	assert ( x . Count () == 8 );
	assert ( x . Weight () == 5150 );
	assert ( !x . Danger () );
	x . Add ( CKnife ( 8 ) );
	os . str ( "" );
	os << x;
	assert ( os . str () == "Suitcase 1x2x3\n"
			 "+-Knife, blade: 7 cm\n"
			 "+-Clothes (red T-shirt)\n"
			 "+-Clothes (black hat)\n"
			 "+-Shoes\n"
			 "+-Clothes (green pants)\n"
			 "+-Clothes (blue jeans)\n"
			 "+-Mobile J3 by: Samsung\n"
			 "+-Mobile Galaxy Note S7 by: Tamtung\n"
			 "\\-Knife, blade: 8 cm\n" );
	assert ( x . Count () == 9 );
	assert ( x . Weight () == 5250 );
	assert ( x . Danger () );
	y . Add ( CKnife ( 7 ) )
	. Add ( CClothes ( "red T-shirt" ) )
	. Add ( CShoes () );
	y . Add ( CMobile ( "Samsung", "Galaxy Note S7" ) );
	y . Add ( CShoes () );
	y . Add ( CClothes ( "blue jeans" ) );
	y . Add ( CClothes ( "black hat" ) );
	y . Add ( CClothes ( "green pants" ) );
	os . str ( "" );
	os << y;
	assert ( os . str () == "Backpack\n"
			 "+-Knife, blade: 7 cm\n"
			 "+-Clothes (red T-shirt)\n"
			 "+-Shoes\n"
			 "+-Mobile Galaxy Note S7 by: Samsung\n"
			 "+-Shoes\n"
			 "+-Clothes (blue jeans)\n"
			 "+-Clothes (black hat)\n"
			 "\\-Clothes (green pants)\n" );
	assert ( y . Count () == 8 );
	assert ( y . Weight () == 4750 );
	assert ( y . Danger () );
	y . Add ( CMobile ( "Samsung", "J3" ) );
	y . Add ( CMobile ( "Tamtung", "Galaxy Note S7" ) );
	y . Add ( CKnife ( 8 ) );
	os . str ( "" );
	os << y;
	assert ( os . str () == "Backpack\n"
			 "+-Knife, blade: 7 cm\n"
			 "+-Clothes (red T-shirt)\n"
			 "+-Shoes\n"
			 "+-Mobile Galaxy Note S7 by: Samsung\n"
			 "+-Shoes\n"
			 "+-Clothes (blue jeans)\n"
			 "+-Clothes (black hat)\n"
			 "+-Clothes (green pants)\n"
			 "+-Mobile J3 by: Samsung\n"
			 "+-Mobile Galaxy Note S7 by: Tamtung\n"
			 "\\-Knife, blade: 8 cm\n" );
	assert ( y . Count () == 11 );
	assert ( y . Weight () == 5150 );
	assert ( y . Danger () );
	assert ( !x . IdenticalContents ( y ) );
	assert ( !y . IdenticalContents ( x ) );
	x . Add ( CMobile ( "Samsung", "Galaxy Note S7" ) );
	assert ( !x . IdenticalContents ( y ) );
	assert ( !y . IdenticalContents ( x ) );
	x . Add ( CShoes () );
// cout << x << endl;
// cout << y << endl;
	assert ( x . IdenticalContents ( y ) );
	assert ( y . IdenticalContents ( x ) );
	assert ( y . IdenticalContents ( y ) );
	assert ( x . IdenticalContents ( x ) );
	return 0;
}
#endif /* __PROGTEST__ */
