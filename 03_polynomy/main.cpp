#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cmath>
#include <cfloat>
#include <cassert>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <complex>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */

#define EPS 1.0e-12

ios_base & dummy_polynomial_manipulator ( ios_base & x )
{ 
  return x;
}

ios_base & ( * ( polynomial_variable ( const string & varName ) ) ) ( ios_base & x )
{
  return dummy_polynomial_manipulator;
}

class CPolynomial
{
private:
    vector<double> vPolynome;
  public:
    // default constructor
    CPolynomial (  ){
    	vPolynome = std::vector<double> (1);
    }
    ~CPolynomial (  ){}

    // operator 																		+
    CPolynomial operator + ( const CPolynomial & b ) const { 
    	CPolynomial tmp;	
    	tmp . vPolynome .resize ( (this -> vPolynome . size () > b . vPolynome .size () ? 
                          this -> vPolynome . size () :
                          b . vPolynome . size ()) ,	0 );
		for (uint i = 0; i < this -> vPolynome . size (); ++i){
    		tmp . vPolynome [i] = this -> vPolynome [i] ;
    	}
 		for (uint i = 0; i < b . vPolynome . size (); ++i){
    		tmp . vPolynome [i] += b . vPolynome [i];
    	}
    	return tmp;
    }
    // operator 																		-
    CPolynomial operator - ( const CPolynomial & b ) const {
    	CPolynomial tmp;
    	tmp . vPolynome . resize ( this -> vPolynome . size () > b . vPolynome .size () ? 
    								this -> vPolynome . size () :
    								b . vPolynome . size () ,	0 );
    	for (uint i = 0; i < this -> vPolynome . size (); ++i){
    		tmp . vPolynome [i] = this -> vPolynome [i] ;
    	}
 		for (uint i = 0; i < b . vPolynome . size (); ++i){
    		tmp . vPolynome [i] -= b . vPolynome [i];
    	}
    	return tmp;
    }
    // operator 																		* (polynomial, double)
    CPolynomial operator * ( const double val ) const {
    	CPolynomial tmp;
    	tmp . vPolynome = this -> vPolynome;
      tmp.vPolynome.resize(this->Degree() + 1);
    	if ( val == 0 )
    		tmp . vPolynome . resize ( 1 , 0 );

    	for (uint i = 0; i < this-> vPolynome . size (); ++i){
    		tmp [i] = (*this)[i] * val;
    	}
    	return tmp;
    }

	CPolynomial operator * ( const CPolynomial & b ) const {
    	CPolynomial prod;
      prod.vPolynome.resize(Degree() + b.Degree() + 2, 0);
    	for (uint i = 0; i < this -> vPolynome . size (); ++i){
    		for (uint j = 0; j < b . vPolynome .size (); ++j){
          // cout << i + j << " " << (*this)[i] << "*" << b[j] << endl;
     			prod [ i + j ] += (*this) [i] * b [j];
    		}
    	}
      prod.print();
    	return prod;
    } 
    // operator 																		==
    bool operator == ( const CPolynomial & b ) const {
    	uint commonSize = this -> vPolynome . size () < b . vPolynome . size () ?
    						 this -> vPolynome . size () : 
    						 b . vPolynome . size () ;

    	// cout << this -> vPolynome . size () << endl;
    	// cout << b . vPolynome . size () << endl;
    	// cout << commonSize << endl;

    	for (uint i = 0; i < commonSize; ++i){
    		if ( fabs ( this -> vPolynome [i] - b . vPolynome [i] ) > EPS ){
    			return false;
    		}
    	}

    	if ( commonSize == this -> vPolynome . size () ){
    		for (uint i = commonSize; i < this -> vPolynome . size () ; ++i){
    			if ( fabs ( this -> vPolynome [i] ) > EPS ){
    				return false;
    			}
    		}
    	} else {
    		for (uint i = commonSize; i < b . vPolynome . size () ; ++i){
    			if ( fabs ( b . vPolynome [i] ) > EPS ){
    				return false;
    			}
    		}
    	}

    	return true;
    } 
    // operator 																		!=
    bool operator != ( const CPolynomial & b ) const {
    	    	uint commonSize = this -> vPolynome . size () < b . vPolynome . size () ?
    						 this -> vPolynome . size () : 
    						 b . vPolynome . size () ;

    	for (uint i = 0; i < commonSize; ++i){
    		if ( fabs ( this -> vPolynome [i] - b . vPolynome [i] ) > EPS ){
    			return true;
    		}
    	}
    	if ( commonSize == this -> vPolynome . size () ){
    		for (uint i = commonSize; i < this -> vPolynome . size () ; ++i){
    			if ( fabs ( this -> vPolynome [i] ) > EPS ){
    				return true;
    			}
    		}
    	} else {
    		for (uint i = commonSize; i < b . vPolynome . size () ; ++i){
    			if ( fabs ( b . vPolynome [i] ) > EPS ){
    				return true;
    			}
    		}
    	}
    	return false;
    } 
    // operator 																		[]
    double & operator [] ( size_t idx ) {
      if (idx >= vPolynome.size())
  		  vPolynome . resize ( idx + 1 , 0 );
    	return vPolynome [ idx ];
    }

    double operator [] ( size_t idx ) const {
      if ( idx >= vPolynome . size () ) return 0;
    	return vPolynome [ idx ];
    }
    // operator 																		()
    double operator () ( const double val ) const {
    	double sum = 0;

    	for (uint i = 0; i < vPolynome . size (); ++i){
    		sum += vPolynome [i] * pow( val, i );
    	}

    	return sum;
    }
    // operator 																		<<
    friend ostream & operator << ( ostream & os, const CPolynomial & pol ){

    	if ( pol . Degree () == 0 ){
        return os << "0";
      }
    	stringstream ss;
    	
    	for ( int i = pol . Degree (); i >= 0 ; --i ){
    		if ( pol . vPolynome [i] != 0 ){

	    		if ( pol . vPolynome [i] < 0 ){
	    			ss << "- ";
	    			if ( pol . vPolynome [i] != -1 ){
	    				ss << fabs ( pol . vPolynome [i] ) << "*";
	    			}
	    		} else {

	    			if ( pol . vPolynome [i] != 1 ){
	    				if ( (uint)i != pol . Degree () )
	    					ss << "+ ";
	    				ss << pol . vPolynome [i] << "*" ;
	    			}

	    		}
	    		ss << "x^" << i << " ";

    		}
    	}

      size_t pos = ss.str().find("*x^0");
      if (pos && pos < ss.str().size()) {
        os << ss.str().substr(0, pos);
      } else {
        os << ss.str().substr(0, ss.str().find_last_of(' '));
      }
      return os;
     //  cout << ss . str ().substr(0, ss.str().find_last_of("*x^0")) << endl;
    	// string str = ss . str ();
    	// if ( pol . vPolynome [0] != 0 ){
    	// 	str . erase ( str . size () - 5, str . size () -1 );
    	// } else {
    	// 	str . erase ( str . size () - 1, str . size () -1 );
    	// }
    	// return os << str;

    }
    // Degree 																			(), returns unsigned value
    uint Degree (  ) const {
    	uint realSize = 0;
    	for (uint i = 0; i < this -> vPolynome . size () ; ++i)
    	{
    		if ( this -> vPolynome [i] ){
    			realSize = i;
    		}
    	}
    	return realSize;
    }


    void print () const{
    	for (uint i = 0; i < vPolynome . size (); ++i)
    	{
    		cout << vPolynome [i] << " ";
    	}
    	cout << endl;
    }

    
};

ostream     & operator <<                   ( ostream & os, const CPolynomial & pol );

#ifndef __PROGTEST__
bool               smallDiff                               ( double            a,
                                                             double            b )
{
   return fabs ( a - b ) < 0.00001 ? true : false ;
}

bool               dumpMatch                               ( const CPolynomial & x,
                                                             const vector<double> & ref )
{
  for (uint i = 0; i < ref . size (); ++i)
  {
    if ( x [i] != ref [i] ) return false;
  }
  return true;
}

int                main                                    ( void )
{
  CPolynomial a, b, c,  t, u, v;
  ostringstream out;

  CPolynomial g;
  g[100] = 4;
  assert(g.Degree() == 100);
  g[100] = 0;
  assert(g.Degree() == 0);

  a[0] = -10;
  a[1] = 3.5;
  a[3] = 1;
  assert ( smallDiff ( a ( 2 ), 5 ) );
  out . str ("");
  out << a;
  assert ( out . str () == "x^3 + 3.5*x^1 - 10" );
  a = a * -2;
  cout << a << endl;
  a.print();
  assert ( a . Degree () == 3
           && dumpMatch ( a, vector<double>{ 20.0, -7.0, -0.0, -2.0 } ) );

  out . str ("");
  out << a;
  assert ( out . str () == "- 2*x^3 - 7*x^1 + 20" );
  out . str ("");
  out << b;
  assert ( out . str () == "0" );
  b[5] = -1;
  out . str ("");
  out << b;
  assert ( out . str () == "- x^5" );
  c = a + b;
  assert ( c . Degree () == 5
           && dumpMatch ( c, vector<double>{ 20.0, -7.0, 0.0, -2.0, 0.0, -1.0 } ) );

  out . str ("");
  out << c;
  assert ( out . str () == "- x^5 - 2*x^3 - 7*x^1 + 20" );
  c = a - b;
  assert ( c . Degree () == 5
           && dumpMatch ( c, vector<double>{ 20.0, -7.0, -0.0, -2.0, -0.0, 1.0 } ) );

  out . str ("");
  out << c;
  assert ( out . str () == "x^5 - 2*x^3 - 7*x^1 + 20" );
  c = a * b;
  a.print();
  b.print();
  cout << c << endl;
  c.print();
  assert ( c . Degree () == 8
           && dumpMatch ( c, vector<double>{ 0.0, -0.0, 0.0, -0.0, -0.0, -20.0, 7.0, 0.0, 2.0 } ) );

  out . str ("");
  out << c;
  cout << c << endl;
  assert ( out . str () == "2*x^8 + 7*x^6 - 20*x^5" );
  assert ( a != b );
  b[5] = 0;

  // a . print ();
  // b . print ();  

  assert ( !(a == b) );
  a = a * 0;
  assert ( a . Degree () == 0
           && dumpMatch ( a, vector<double>{ 0.0 } ) );


  assert ( a == b );

  // t = u * v;
  // cout << t << endl;

  // t = u - v;
  // cout << t << endl;

  // u[5] = 3;
  // u[3] = 1;
  // u[8] = 4;

  // v[5] = 3;
  // v[3] = 1;
  // v[8] = 4;

  // t = u - v;
  // t . print ();
  // cout << t << endl;
  // assert ( v == u );


  // bonus
  // a[2] = 4;
  // a[1] = -3;
  // b[3] = 7;
  // out . str ("");
  // out << polynomial_variable ( "y" ) << "a=" << a << ", b=" << b;
  // assert ( out . str () == "a=4*y^2 - 3*y^1, b=7*y^3" );

  // out . str ("");
  // out << polynomial_variable ( "test" ) << c;
  // assert ( out . str () == "2*test^8 + 7*test^6 - 20*test^5" );
  return 0;
}
#endif /* __PROGTEST__ */
