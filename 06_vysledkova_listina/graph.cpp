// =========================================================================================
class CGraph
{
	struct node
	{
		int s_refCnt;
		vector<node*> s_ptrs;
		node(){
			s_refCnt = 0;
		}
		node (const node & src){
			// isVisited = src . isVisited;
			s_ptrs = src . s_ptrs;
			s_refCnt = src . s_refCnt;
		}
	};

	unordered_map<string, node> m_graph;
	list<string> m_list;

public:
	CGraph (){}
	// pridavam zapas na zaklade toho kto vyhral. pri remize pridam (W,L) ale aj (L,W) 
	bool addToGraph( const string & winner, const string & loser )
	{ 
		// check whether there is the same match
		if ( m_graph . find (loser) != m_graph.end() ){
			node * ptr = & m_graph[loser];
			for (auto x : m_graph[winner] . s_ptrs){
				if (x == ptr) /*return false;*/
				// tento throw tu tusim ani netreba
					throw DuplicateMatchException();
			}
		}
		m_graph[winner] . s_ptrs . push_back( & m_graph[loser] );
		m_graph[loser] . s_refCnt ++;
		return true;
	}

// volam ked chcem zistit ci je graf riesitelny. zaroven si vysledky pridavam do listu, tu by sa teoreticky dala
// spravit dalsia funkcia, ktora by spravila to iste s rozdielom ze by to vracalo(?) list, s tym ze by throwovala tu hnusnu exception.
	bool isOrdered ()
	{
		string found;
		int cnt = 0;

		while( ! m_graph . empty() ){
			for( auto itMap : m_graph ){
				if( itMap.second . s_refCnt == 0 ){
					cnt++;
					found = itMap.first;
				}
			}
			if( cnt == 1 ){
				for( auto vecPtrs : m_graph[found] . s_ptrs ){
					(vecPtrs -> s_refCnt) --;
				}
				m_list.push_back( found );
				m_graph . erase( found );
				cnt = 0;
			} else {
				return false;
			}
		}
		return true;
	}
// treba najskor zavolat funkciu isOrdered()
	list<string> getList(){
		return m_list;
	}
};

// =========================================================================================