#ifndef __PROGTEST__
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <set>
#include <list>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <stack>
#include <queue>
#include <deque>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <functional>
using namespace std;
#endif /* __PROGTEST__ */

class OrderingDoesNotExistException/*: public exception*/
{
	// virtual const char * 		what () const /*throw()*/noexcept
	// {
	// 	return "ordering does not exist!";
	// }
};

class DuplicateMatchException/*: public exception*/
{
	// virtual const char * 		what () const throw()noexcept
	// {
	// 	return "duplicate match!";
	// }
};

// =========================================================================================
class CGraph
{
	struct node
	{
		int s_refCnt;
		vector<node*> s_ptrs;
		node(){
			s_refCnt = 0;
		}
		node (const node & src){
			// isVisited = src . isVisited;
			s_ptrs = src . s_ptrs;
			s_refCnt = src . s_refCnt;
		}
	};

	unordered_map<string, node> m_graph;
	list<string> m_list;

public:
	CGraph (){}
	// pridavam zapas na zaklade toho kto vyhral. pri remize pridam (W,L) ale aj (L,W) 
	bool addToGraph( const string & winner, const string & loser )
	{ 
		// check whether there is the same match
		if ( m_graph . find (loser) != m_graph.end() ){
			node * ptr = & m_graph[loser];
			for (auto x : m_graph[winner] . s_ptrs){
				if (x == ptr) /*return false;*/
				// tento throw tu tusim ani netreba
					throw DuplicateMatchException();
			}
		}
		m_graph[winner] . s_ptrs . push_back( & m_graph[loser] );
		m_graph[loser] . s_refCnt ++;
		return true;
	}

// volam ked chcem zistit ci je graf riesitelny. zaroven si vysledky pridavam do listu, tu by sa teoreticky dala
// spravit dalsia funkcia, ktora by spravila to iste s rozdielom ze by to vracalo(?) list, s tym ze by throwovala tu hnusnu exception.
	bool isOrdered ()
	{
		string found;
		int cnt = 0;

		while( ! m_graph . empty() ){
			for( auto itMap : m_graph ){
				if( itMap.second . s_refCnt == 0 ){
					cnt++;
					found = itMap.first;
				}
			}
			if( cnt == 1 ){
				for( auto vecPtrs : m_graph[found] . s_ptrs ){
					(vecPtrs -> s_refCnt) --;
				}
				m_list.push_back( found );
				m_graph . erase( found );
				cnt = 0;
			} else {
				return false;
			}
		}
		return true;
	}
// treba najskor zavolat funkciu isOrdered()
	list<string> getList(){
		return m_list;
	}
};

// =========================================================================================
// =========================================================================================
template <typename _M>
class CContest
{
private:
	struct SMatch
	{
		SMatch(){}
		SMatch (string c1, string c2, _M res)
			:s_c1(c1),s_c2(c2),s_result(res)
		{}
		string s_c1, s_c2;
		_M s_result;
	};
	// unordered_set<SMatch> m_sMatches;
	vector<SMatch> m_sMatches;

public:
	// default constructor
											CContest								( void )
	{
	}
	// destructor
											~CContest							( void )
	{
	}
	// AddMatch ( contestant1, contestant2, result )
	CContest							& 	AddMatch 							( string contestant1,
																					  string contestant2,
																					  _M 		result)
	{
		// if( m_sMatches . find(SMatch(contestant1,contestant2,result)) != m_sMatches.end() ){
		for (auto i : m_sMatches)
		{
			if ( i.s_c1==contestant1 && i.s_c2==contestant2 )
				throw DuplicateMatchException();
			if ( i.s_c2==contestant1 && i.s_c1==contestant2 )
				throw DuplicateMatchException();
		}
		m_sMatches . push_back( SMatch(contestant1,contestant2,result) );
		return *this;
	}
	// IsOrdered ( comparator )
	template <class T>
	bool									IsOrdered							( T comparator ) const
	{
		//zavolaj komarator na cmatch =  comparator(result)
		CGraph g;
		for(auto x : m_sMatches){
			int res = comparator(x . s_result);

			if( res == 0 ){
				g . addToGraph(x.s_c1, x.s_c2);
				g . addToGraph(x.s_c2, x.s_c1);
			} else if( res > 0 ) {
				g . addToGraph(x.s_c1, x.s_c2);
			} else {
				g . addToGraph(x.s_c2, x.s_c1);
			}
		}

		if( g . isOrdered() ){
			return true;
		} else {
			return false;
		}
		return true;
	}
	// Results ( comparator )
	template <class T>
	list<string>						Results								( T comparator ) const
	{
		CGraph g;
		for(auto x : m_sMatches){
			int res = comparator(x . s_result);

			if( res == 0 ){
				g . addToGraph(x.s_c1, x.s_c2);
				g . addToGraph(x.s_c2, x.s_c1);
			} else if( res > 0 ) {
				g . addToGraph(x.s_c1, x.s_c2);
			} else {
				g . addToGraph(x.s_c2, x.s_c1);
			}
		}

		if( g . isOrdered() ){
			return g . getList();
		} else {
			throw OrderingDoesNotExistException();
		}
	}
};

#ifndef __PROGTEST__

// =========================================================================================
// =========================================================================================
struct CMatch
{
public:
									CMatch                        ( int			a, 
																			  int			b )
									: m_A ( a ), 
									  m_B ( b )
	{
	}
	
	int							  m_A;
	int							  m_B;
};

class HigherScoreThreshold
{
public:
									HigherScoreThreshold				( int diffAtLeast )
									: m_DiffAtLeast ( diffAtLeast )
	{
	}
	int							operator ()							( const CMatch & x ) const
	{
		return ( x . m_A > x . m_B + m_DiffAtLeast ) - ( x . m_B > x . m_A + m_DiffAtLeast );
	}
private:
	int							m_DiffAtLeast;    
};
// =========================================================================================
int								HigherScore							( const CMatch & x )
{
	return ( x . m_A > x . m_B ) - ( x . m_B > x . m_A );
}




int                main                                    ( void )
{
  CContest<CMatch>  x;
  
  x . AddMatch ( "C++", "Pascal", CMatch ( 10, 3 ) )
    . AddMatch ( "C++", "Java", CMatch ( 8, 1 ) )
    . AddMatch ( "Pascal", "Basic", CMatch ( 40, 0 ) )
    . AddMatch ( "Java", "PHP", CMatch ( 6, 2 ) )
    . AddMatch ( "Java", "Pascal", CMatch ( 7, 3 ) )
    . AddMatch ( "PHP", "Basic", CMatch ( 10, 0 ) );
    
  
  assert ( ! x . IsOrdered ( HigherScore ) );
  try
  {
    list<string> res = x . Results ( HigherScore );
    assert ( "Exception missing!" == NULL );
  }
  catch ( const OrderingDoesNotExistException & e )
  {
  }
  catch ( ... )
  {
    assert ( "Invalid exception thrown!" == NULL );
  }

  x . AddMatch ( "PHP", "Pascal", CMatch ( 3, 6 ) ); 

  assert ( x . IsOrdered ( HigherScore ) );
  try
  {
    list<string> res = x . Results ( HigherScore );
    assert ( ( res == list<string>{ "C++", "Java", "Pascal", "PHP", "Basic" } ) );
  }
  catch ( ... )
  {
    assert ( "Unexpected exception!" == NULL );
  }

  
  assert ( ! x . IsOrdered ( HigherScoreThreshold ( 3 ) ) );
  try
  {
    list<string> res = x . Results ( HigherScoreThreshold ( 3 ) );
    assert ( "Exception missing!" == NULL );
  }
  catch ( const OrderingDoesNotExistException & e )
  {
  }
  catch ( ... )
  {
    assert ( "Invalid exception thrown!" == NULL );
  }
  
  assert ( x . IsOrdered ( [] ( const CMatch & x )
  {
    return ( x . m_A < x . m_B ) - ( x . m_B < x . m_A ); 
  } ) );
  try
  {
    list<string> res = x . Results ( [] ( const CMatch & x )
    {
      return ( x . m_A < x . m_B ) - ( x . m_B < x . m_A ); 
    } );
    assert ( ( res == list<string>{ "Basic", "PHP", "Pascal", "Java", "C++" } ) );
  }
  catch ( ... )
  {
    assert ( "Unexpected exception!" == NULL );
  }
  
  CContest<bool>  y;
  
  y . AddMatch ( "Python", "PHP", true )
    . AddMatch ( "PHP", "Perl", true )
    . AddMatch ( "Perl", "Bash", true )
    . AddMatch ( "Bash", "JavaScript", true )
    . AddMatch ( "JavaScript", "VBScript", true );
  
  assert ( y . IsOrdered ( [] ( bool v )
  {
    return v ? 10 : - 10;
  } ) );
  try
  {
    list<string> res = y . Results ( [] ( bool v )
    {
      return v ? 10 : - 10;
    });
    assert ( ( res == list<string>{ "Python", "PHP", "Perl", "Bash", "JavaScript", "VBScript" } ) );
  }
  catch ( ... )
  {
    assert ( "Unexpected exception!" == NULL );
  }
    
  y . AddMatch ( "PHP", "JavaScript", false );
  assert ( !y . IsOrdered ( [] ( bool v )
  {
    return v ? 10 : - 10;
  } ) );
  try
  {
    list<string> res = y . Results ( [] ( bool v )
    {
      return v ? 10 : - 10;
    } );
    assert ( "Exception missing!" == NULL );
  }
  catch ( const OrderingDoesNotExistException & e )
  {
  }
  catch ( ... )
  {
    assert ( "Invalid exception thrown!" == NULL );
  }
  
  try
  {
    y . AddMatch ( "PHP", "JavaScript", false );
    assert ( "Exception missing!" == NULL );
  }
  catch ( const DuplicateMatchException & e )
  {
  }
  catch ( ... )
  {
    assert ( "Invalid exception thrown!" == NULL );
  }
  
  try
  {
    y . AddMatch ( "JavaScript", "PHP", true );
    assert ( "Exception missing!" == NULL );
  }
  catch ( const DuplicateMatchException & e )
  {
  }
  catch ( ... )
  {
    assert ( "Invalid exception thrown!" == NULL );
  }
  return 0;
}
#endif /* __PROGTEST__ */
