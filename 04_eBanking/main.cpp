#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cctype>
#include <cmath>
#include <iostream>
#include <sstream>

#include <string>

using namespace std;
#endif /* __PROGTEST__ */
/*
template <class T >
void reall( T * & src, int & size ){

    if ( size == 0 ) {
        size = 1;
        src = new T[1];
    } else {

        T * tmp = new T [ size * 2 ]();
        for (int i = 0; i < size; ++i)
        {
            tmp[i] = src[i];
        }

        delete [] src;
        src = tmp;
        size *= 2;
    }
}*/



class 																	CTransaction
{
public:

    int amount;
    bool isPositive;
    char * account;
    char * sign;

    // nastav hodnoty
    CTransaction (){
        amount = 0;
        isPositive = true;
        account = nullptr;
        sign = nullptr;
    }

    // vypln hodnotami
    CTransaction ( int amo, const char * acc, const char * sig, bool isPos ){
        amount = amo;
        isPositive = isPos;

        account = new char [ strlen(acc) + 1 ]();
        strncpy ( account, acc, strlen ( acc ) + 1 );

        sign = new char [ strlen(sig) + 1 ]();
        strncpy ( sign, sig, strlen ( sig ) + 1 );
    }

    // kopirujuci konstrukor
    CTransaction ( const CTransaction & src ){
        if (this != &src) {
            amount = src . amount;
            isPositive = src . isPositive;

            account = new char [ strlen (src . account) + 1 ]();
            strncpy ( account, src . account, strlen ( src . account ) + 1 );

            sign = new char [ strlen (src . sign) + 1 ]();
            strncpy ( sign, src . sign, strlen ( src . sign ) + 1 );
        }

    }

    ~CTransaction (){
        delete [] account;
        delete [] sign;
    }


    CTransaction & operator = ( const CTransaction & src ) {
        if (this != &src) {
            amount = src . amount;
            isPositive = src . isPositive;

// !!!!!!!!!!!!!!!! ta ista chyba

            delete[] account;

            account = new char [ strlen (src . account) + 1 ]();
            strncpy ( account, src . account, strlen ( src . account ) + 1 );

            delete[] sign;

            sign = new char [ strlen (src . sign) + 1 ]();
            strncpy ( sign, src . sign, strlen ( src . sign ) + 1 );
        }
        return *this;
    }
};










class 																	CAccount
{
private:

public:
    char * name;
    int balance;

    int transCnt;
    int transSize;

    CTransaction * arrTrans;

    CAccount (){
        name = nullptr;
        balance = 0;

        transCnt = 0;
        transSize = 0;

        arrTrans = nullptr;
    }

    CAccount ( const char * nam, int bal ){
        name = new char [ strlen (nam) + 1 ]();
        strncpy(name, nam, strlen(nam) + 1 );
        balance = bal;

        transCnt = 0;
        transSize = 0;

        arrTrans = nullptr;
    }

    // kopirujuci konstruktor
    CAccount ( const CAccount & src ){
        if (this != &src) {

            name = new char [ strlen (src.name) + 1 ]();
            strncpy( name, src.name, strlen (src.name) );

            balance = src . balance;

            transCnt = src . transCnt;
            transSize = src . transSize;

            arrTrans = new CTransaction [ src . transSize ]();
            for (int i = 0; i < transCnt ; ++i)
            {
                arrTrans [i] = src . arrTrans [i];
            }
        }
    }


    CAccount & operator = ( const CAccount &src ) {
        if (this != &src) {
            delete[] arrTrans;
            delete[] name;

            name = new char [ strlen(src.name) + 1 ];
            strncpy ( name, src . name, strlen(src . name) + 1 );

            balance = src . balance;

            transCnt = src . transCnt;
            transSize = src . transSize;

            arrTrans = new CTransaction [ src . transSize ];

            for (int i = 0; i < transCnt ; ++i)
            {
                arrTrans [i] = src . arrTrans [i];
            }
        }
        return *this;
    }



    ~CAccount () {
        delete [] name;
        delete [] arrTrans;
    }

    void makeTransaction ( int amount, const char * & account, const char * & sign, bool isPositive ){

        if ( transCnt == transSize ){
            // reall < CTransaction > ( arrTrans, transSize );



            if ( transSize == 0 ) {

// !!!!!!!!!!!!!!!!!!!!! najskor deletni pole kam to zapisujes

                delete [] arrTrans;
                transSize = 1;
                arrTrans = new CTransaction [1];    
            } else {

                CTransaction * tmp = new CTransaction [ transSize * 2 ];
                for (int i = 0; i < transCnt; ++i)
                {
                    tmp[i] = arrTrans[i];
                }

                delete [] arrTrans;
                arrTrans = tmp;
                transSize *= 2;
            }




        }

        arrTrans [transCnt] = CTransaction ( amount, account, sign, isPositive );
        transCnt++;
    }

    void trim (){
        if ( transCnt != 0 ){
            // set balance
            balance = Balance();

            delete [] arrTrans;
            arrTrans = nullptr;

            transCnt = 0;
            transSize = 0;

        }
    }

    const char * getName () const{
        return name;
    }

    int Balance () const {
        int sum = balance;

        for (int i = 0; i < transCnt; ++i)
        {
            if ( arrTrans [i] . isPositive ){
                sum += arrTrans [i] . amount;
            } else {
                sum -= arrTrans [i] . amount;
            }
        }
        return sum;
    }
    // "123456:\n   1000\n - 300, to: 987654, sign: XAbG5uKz6E=\n - 2890, to: 987654, sign: AbG5uKz6E=\n = -2190\n"
    friend ostream & operator << ( ostream & os, const CAccount & src ){

        os << src . name << ":\n";

        os << "   " << src . balance << "\n";

        for (int i = 0; i < src . transCnt; ++i)
        {
            if ( src . arrTrans [i] . isPositive == true ){
                os  << " + " << src . arrTrans [i] . amount
                    << ", from: " << src . arrTrans [i] . account
                    << ", sign: " << src . arrTrans [i] . sign;
            } else {
                os  << " - " << src . arrTrans [i] . amount
                    << ", to: " << src . arrTrans [i] . account
                    << ", sign: " << src . arrTrans [i] . sign;
            }
            os << "\n";
        }
        os << " = " << src . Balance() << "\n";
        return os;
    }

};











class 																	CBank
{
public:

    // default constructor
    CBank (){
        ptr = new CFilial( );
    }

    // copy constructor
    CBank ( const CBank & src )
    {
        attach( src . ptr );

            // if ( --ptr -> RefCnt == 0 ){
            //     delete [] ptr;
            // }
        // accountSize = src . accountSize;
        // accountCnt = src . accountCnt;
 
        // account = new CAccount [ src . accountSize ]();
        // for (int i = 0; i < src . accountCnt; ++i)
        // {
        //     account [i] = src . account [i];
        // }
    }

    // destructor
    ~CBank (){
        if ( --ptr -> RefCnt == 0 ){
            delete ptr;
        }
        // detach();
    }

    // operator =
    CBank & operator = (  CBank src ) {
        if (this != &src) {
            
            // attach( src . ptr );
            swap ( ptr, src . ptr );

            // if ( --ptr -> RefCnt == 0 ){
            //     delete [] ptr;
            // }
            // // attach ( src . ptr );
            // ptr = src . ptr;
            // ptr -> RefCnt++;

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! najskor deletni tam kam to pridavas
            // delete [] account;

            // accountSize = src . accountSize;
            // accountCnt = src . accountCnt;

            // account = new CAccount [ src . accountSize ]();
            // for (int i = 0; i < accountCnt ; ++i)
            // {
            //     account [i] = src . account [i];
            // }
        }
        return *this;
    }

    bool   NewAccount    ( const char * accID,
                           int          initialBalance ){
        detach ();

        for (int i = 0; i < ptr -> accountCnt; ++i)
        {
            if ( strncmp ( ptr -> account [i] . getName(), accID, strlen(accID) ) == 0 ) return false;
        }

        if ( ptr -> accountSize == ptr -> accountCnt ){

            if ( ptr -> accountSize == 0 ) {
                delete [] ptr -> account;
                ptr -> accountSize = 1;
                ptr -> account = new CAccount[1];
            } else {

                CAccount * tmp = new CAccount [ ptr -> accountSize * 2 ];
                for (int i = 0; i < ptr -> accountCnt; ++i)
                {
                    tmp[i] = ptr -> account[i];
                }

                delete [] ptr -> account;
                ptr -> account = tmp;
                ptr -> accountSize *= 2;
            }
        }

        ptr -> account[ ptr -> accountCnt ] = CAccount( accID, initialBalance );
        ptr -> accountCnt++;

        return true;
        
    }

    bool   Transaction   ( const char * debAccID,
                           const char * credAccID,
                           int          amount,
                           const char * signature ){


        detach();
        // nezabudni poriesit inicialiaaciu pre velkost a vsetko viac menej co je tu ale dajako s korelaciou na CFilial

        int debIndex = -1;
        int credIndex = -1;

        if ( strncmp ( debAccID, credAccID, strlen (credAccID) ) == 0 )
            return false;

        // ucty musia existovat
        for (int i = 0; i < ptr -> accountCnt; ++i)
        {
            if ( strncmp ( ptr -> account [i] . getName(), debAccID, strlen(debAccID) ) == 0 ){
                debIndex = i;
            }
            if ( strncmp ( ptr -> account [i] . getName(), credAccID, strlen(credAccID) ) == 0 ){
                credIndex = i;
            }

            if ( debIndex >= 0 && credIndex >= 0 ){

                // vytvor transakciu
                ptr -> account [debIndex] . makeTransaction ( amount, credAccID, signature, false );
                ptr -> account [credIndex] . makeTransaction ( amount, debAccID, signature, true );

                return true;
            }
        }
        return false;
    }

    bool   TrimAccount   ( const char * accID ){

        detach();
        for (int i = 0; i < ptr -> accountCnt; ++i)
        {
            if ( strncmp ( ptr -> account [i] . getName(), accID, strlen(accID) ) == 0 ){
                ptr -> account [i] . trim();
                return true;
            }
        }
        // nenasiel som
        return false;
    }

    CAccount Account ( const char * accID ){

        // hladam ucet s rovnakym menom, kde hladam transakciu kde spravim cely Balance()
        for (int i = 0; i < ptr -> accountCnt	; ++i)
        {
            if ( strncmp ( ptr -> account [i] . getName (), accID, strlen(accID) ) == 0 ){
                return ptr -> account [i];
            }
        }
        throw ( "non existing account or else rubbish" );
    }


    void printAll (){
        cout << "******** printing whole class ******** " << endl;
        for (int i = 0; i < ptr -> accountCnt; ++i)
        {
            cout << ptr -> account [i] . name << endl;
            for (int j = 0; j < ptr -> account [i] . transCnt; ++j)
            {
                cout << "account: " << ptr -> account [i] . arrTrans [j] . account
                     << " amount: " << ptr -> account [i] . arrTrans [j] . amount
                     << " positive: " << ptr -> account [i] . arrTrans [j] . isPositive
                     << " sign: " << ptr -> account [i] . arrTrans [j] . sign
                     << endl;
            }
        }
    }


private:
    struct CFilial 
    {

        CAccount * account;

        int accountSize;
        int accountCnt;
        int RefCnt;

        CFilial ( )
        {
            accountSize = 0;
            accountCnt = 0;

            account = nullptr;

            RefCnt = 1;
        }
        CFilial ( int newMaxSize ) 
        {
            accountSize = newMaxSize;
            accountCnt = 0;

            account = new CAccount [ newMaxSize ];

            RefCnt = 1;
        }
        ~CFilial ( )
        {
            delete [] account;
        }
    };

    CFilial * ptr;

    void attach ( CFilial * p )
    {
        ptr = p;
        ptr -> RefCnt++;
    }
    void detach ( )
    {
    if ( ptr -> RefCnt > 1 )
      { // data block is shared among more than one instance of CStrAuto. Create own block
        // that can be modified without affecting the other CStrAuto instances.

        CFilial * tmp = new CFilial ( ptr -> accountSize );

        for (int i = 0; i < ptr -> accountCnt; ++i)
        {
            tmp -> account [i] = ptr -> account[i];
        }

        tmp -> accountSize = ptr -> accountSize;
        tmp -> accountCnt = ptr -> accountCnt;

        // tmp is a separate data block for use in our instance. Disconnect from the source
        ptr -> RefCnt --; //data?
        ptr = tmp;
      }
    }
};


#ifndef __PROGTEST__
int main ( void )
{

    ostringstream os;
    char accCpy[100], debCpy[100], credCpy[100], signCpy[100];
    CBank x0;
    assert ( x0 . NewAccount ( "123456", 1000 ) );
    assert ( x0 . NewAccount ( "987654", -500 ) );
    
    
    assert ( x0 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
    // assert ( x0 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
    assert ( x0 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
    assert ( x0 . NewAccount ( "111111", 5000 ) );
    assert ( x0 . Transaction ( "111111", "987654", 290, "Okh6e+8rAiuT5=" ) );
x0.printAll();
cout << x0 . Account ( "123456" ). Balance ( ) << endl;
    assert ( x0 . Account ( "123456" ). Balance ( ) ==  -2190 );
    assert ( x0 . Account ( "987654" ). Balance ( ) ==  2980 );
    assert ( x0 . Account ( "111111" ). Balance ( ) ==  4710 );
    os . str ( "" );
    os << x0 . Account ( "123456" );
    assert ( ! strcmp ( os . str () . c_str (), "123456:\n   1000\n - 300, to: 987654, sign: XAbG5uKz6E=\n - 2890, to: 987654, sign: AbG5uKz6E=\n = -2190\n" ) );
    os . str ( "" );
    os << x0 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 290, from: 111111, sign: Okh6e+8rAiuT5=\n = 2980\n" ) );
    os . str ( "" );
    os << x0 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 290, to: 987654, sign: Okh6e+8rAiuT5=\n = 4710\n" ) );

    assert ( x0 . TrimAccount ( "987654" ) );
    assert ( x0 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );
    os . str ( "" );
    os << x0 . Account ( "987654" );
// cout << x0 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   2980\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 3103\n" ) );

    CBank x2;
    strncpy ( accCpy, "123456", sizeof ( accCpy ) );
    assert ( x2 . NewAccount ( accCpy, 1000 ));
    strncpy ( accCpy, "987654", sizeof ( accCpy ) );
    assert ( x2 . NewAccount ( accCpy, -500 ));
    strncpy ( debCpy, "123456", sizeof ( debCpy ) );
    strncpy ( credCpy, "987654", sizeof ( credCpy ) );
    strncpy ( signCpy, "XAbG5uKz6E=", sizeof ( signCpy ) );
    assert ( x2 . Transaction ( debCpy, credCpy, 300, signCpy ) );
    strncpy ( debCpy, "123456", sizeof ( debCpy ) );
    strncpy ( credCpy, "987654", sizeof ( credCpy ) );
    strncpy ( signCpy, "AbG5uKz6E=", sizeof ( signCpy ) );
    assert ( x2 . Transaction ( debCpy, credCpy, 2890, signCpy ) );
    strncpy ( accCpy, "111111", sizeof ( accCpy ) );
    assert ( x2 . NewAccount ( accCpy, 5000 ));
    strncpy ( debCpy, "111111", sizeof ( debCpy ) );
    strncpy ( credCpy, "987654", sizeof ( credCpy ) );
    strncpy ( signCpy, "Okh6e+8rAiuT5=", sizeof ( signCpy ) );
    assert ( x2 . Transaction ( debCpy, credCpy, 2890, signCpy ) );
    assert ( x2 . Account ( "123456" ). Balance ( ) ==  -2190 );
    assert ( x2 . Account ( "987654" ). Balance ( ) ==  5580 );
    assert ( x2 . Account ( "111111" ). Balance ( ) ==  2110 );
    os . str ( "" );
    os << x2 . Account ( "123456" );
    assert ( ! strcmp ( os . str () . c_str (), "123456:\n   1000\n - 300, to: 987654, sign: XAbG5uKz6E=\n - 2890, to: 987654, sign: AbG5uKz6E=\n = -2190\n" ) );
    os . str ( "" );
    os << x2 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n = 5580\n" ) );
    os . str ( "" );
    os << x2 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n = 2110\n" ) );
    assert ( x2 . TrimAccount ( "987654" ) );
    strncpy ( debCpy, "111111", sizeof ( debCpy ) );
    strncpy ( credCpy, "987654", sizeof ( credCpy ) );
    strncpy ( signCpy, "asdf78wrnASDT3W", sizeof ( signCpy ) );
    assert ( x2 . Transaction ( debCpy, credCpy, 123, signCpy ) );
    os . str ( "" );
    os << x2 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   5580\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 5703\n" ) );

    CBank x4;
    assert ( x4 . NewAccount ( "123456", 1000 ) );
    assert ( x4 . NewAccount ( "987654", -500 ) );
    assert ( !x4 . NewAccount ( "123456", 3000 ) );
    assert ( !x4 . Transaction ( "123456", "666", 100, "123nr6dfqkwbv5" ) );
    assert ( !x4 . Transaction ( "666", "123456", 100, "34dGD74JsdfKGH" ) );

    x4 . printAll();

    assert ( !x4 . Transaction ( "123456", "123456", 100, "Juaw7Jasdkjb5" ) );
    try
    {
        x4 . Account ( "666" ). Balance ( );
        assert ( "Missing exception !!" == NULL );
    }
    catch ( const char * msg )
    {
        cout << msg << endl;
    }
    try
    {
        os << x4 . Account ( "666" ). Balance ( );
        assert ( "Missing exception !!" == NULL );
    }
    catch ( const char * msg )
    {
        cout << msg << endl;
    }
    assert ( !x4 . TrimAccount ( "666" ) );

    CBank x6;
    assert ( x6 . NewAccount ( "123456", 1000 ) );
    assert ( x6 . NewAccount ( "987654", -500 ) );
    assert ( x6 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
    assert ( x6 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
    assert ( x6 . NewAccount ( "111111", 5000 ) );
    assert ( x6 . Transaction ( "111111", "987654", 2890, "Okh6e+8rAiuT5=" ) );
    CBank x7 ( x6 );
    assert ( x6 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );
    assert ( x7 . Transaction ( "111111", "987654", 789, "SGDFTYE3sdfsd3W" ) );
    assert ( x6 . NewAccount ( "99999999", 7000 ) );
    assert ( x6 . Transaction ( "111111", "99999999", 3789, "aher5asdVsAD" ) );
    assert ( x6 . TrimAccount ( "111111" ) );
    assert ( x6 . Transaction ( "123456", "111111", 221, "Q23wr234ER==" ) );
    os . str ( "" );
    os << x6 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   -1802\n + 221, from: 123456, sign: Q23wr234ER==\n = -1581\n" ) );
    os . str ( "" );
    os << x6 . Account ( "99999999" );
    assert ( ! strcmp ( os . str () . c_str (), "99999999:\n   7000\n + 3789, from: 111111, sign: aher5asdVsAD\n = 10789\n" ) );
    os . str ( "" );
    os << x6 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 5703\n" ) );
    os . str ( "" );
    os << x7 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n - 789, to: 987654, sign: SGDFTYE3sdfsd3W\n = 1321\n" ) );
    try
    {
        os << x7 . Account ( "99999999" ). Balance ( );
        assert ( "Missing exception !!" == NULL );
    }
    catch ( const char * msg )
    {
        cout << msg << endl;
    }
    os . str ( "" );
    os << x7 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n + 789, from: 111111, sign: SGDFTYE3sdfsd3W\n = 6369\n" ) );

    CBank x8;
    CBank x9;
    assert ( x8 . NewAccount ( "123456", 1000 ) );
    assert ( x8 . NewAccount ( "987654", -500 ) );
    assert ( x8 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
    assert ( x8 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
    assert ( x8 . NewAccount ( "111111", 5000 ) );
    assert ( x8 . Transaction ( "111111", "987654", 2890, "Okh6e+8rAiuT5=" ) );
    x9 = x8;
    assert ( x8 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );
    assert ( x9 . Transaction ( "111111", "987654", 789, "SGDFTYE3sdfsd3W" ) );
    assert ( x8 . NewAccount ( "99999999", 7000 ) );
    assert ( x8 . Transaction ( "111111", "99999999", 3789, "aher5asdVsAD" ) );
    assert ( x8 . TrimAccount ( "111111" ) );
    os . str ( "" );
    os << x8 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   -1802\n = -1802\n" ) );
    os . str ( "" );
    os << x9 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n - 789, to: 987654, sign: SGDFTYE3sdfsd3W\n = 1321\n" ) );




// CBank xa;

// size_t ROUNDS = 1;
//      for (size_t i = 0; i < ROUNDS; i++) {
//          assert(xa.NewAccount(std::to_string(i).c_str(), 0));
//          os << xa.Account ( std::to_string(i).c_str() );
//          os . str ( "" );
//      }
//      std::cout << "Inserted" << std::endl;
//      for (size_t i = 0; i < ROUNDS; i++) {

//          // xa . printAll();
//          assert((xa.Transaction(std::to_string(i).c_str(), std::to_string(ROUNDS - i - 1).c_str(), ROUNDS, std::to_string(i).c_str()) && i != ROUNDS - i - 1) || (std::cout << i << std::endl && false));
        
//          os << xa.Account ( std::to_string(i).c_str() );
//          os . str ( "" );
//          os << xa.Account ( std::to_string(ROUNDS - i - 1).c_str() );
//          os . str ( "" );
//      }

//      std::cout << "T1" << std::endl;
//      CBank x1 = xa ;
//      for (size_t i = 0; i < ROUNDS; i++) {
//          assert((x1.Account(std::to_string(i).c_str()).Balance() == 0) || (std::cout << i << " " <<  x1.Account(std::to_string(i).c_str()).Balance() << std::endl && false));
        
//      }
//      std::cout << "Copy T2" << std::endl;
    
//      for (size_t i = 0; i < ROUNDS / 2; i++) {
//          assert((x1.Transaction(std::to_string(i).c_str(), std::to_string(ROUNDS - i - 1).c_str(), ROUNDS, std::to_string(i).c_str()) && i != ROUNDS - i - 1) || (std::cout << i << std::endl && false));
//          assert(x1.TrimAccount(std::to_string(i).c_str()));
//      }
//      std::cout << "Trim" << std::endl;
//      for (size_t i = 0; i < ROUNDS / 2; i++) {
//          if (i < ROUNDS / 2) {
//              assert((x1.Account(std::to_string(i).c_str()).Balance() == (int)-ROUNDS) || (std::cout << i << " " <<  x1.Account(std::to_string(i).c_str()).Balance() << std::endl && false));
//          } else {
//              assert((x1.Account(std::to_string(i).c_str()).Balance() == (int)ROUNDS) || (std::cout << i << " " <<  x1.Account(std::to_string(i).c_str()).Balance() << std::endl && false));
//          }
//      }
//      std::cout << "Done" << std::endl;





// // my asserts

//     CBank x10;

// //    assert ( x10 . NewAccount ( "123", 1000 ) );
// //    assert ( x10 . NewAccount ( "987", 1000 ) );
// //    assert ( ! x10 . NewAccount ( "987", 1000 ) );
// //
// //

//     assert ( ! x10 . Transaction ( "", "987", 300, "XAbG5uKz6E=" ) );

//     for (int i = 0; i < 1000; ++i)
//     {
//         string tmp = "cat"+to_string(i);
//         char tab2[1024];
//         strncpy(tab2, tmp.c_str(), sizeof(tab2));
//         tab2[sizeof(tab2) - 1] = 0;
//         assert ( x10 . NewAccount ( tab2, 1000 ) );
//     }

//         assert ( !x10 . Transaction ( "123", "987", 300, "XAbG5uKz6E=" ) );
//         for (int i = 0; i < 500; ++i)
//             assert (! x10 . Transaction ( "123", "987", 300, "XAbG5uKz6E=" ) );





    return 0;
}
#endif /* __PROGTEST__ */
