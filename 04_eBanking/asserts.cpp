/*Vlastne testovacie asserty - medzne hodnoty*/
#include <string>
 	size_t ROUNDS = 10000;
 	for (size_t i = 0; i < ROUNDS; i++) {
 		assert(x0.NewAccount(std::to_string(i).c_str(), 0));
 		os << x0.Account ( std::to_string(i).c_str() );
 		os . str ( "" );
 	}
 	std::cout << "Inserted" << std::endl;
 	for (size_t i = 0; i < ROUNDS; i++) {
 		assert((x0.Transaction(std::to_string(i).c_str(), std::to_string(ROUNDS - i - 1).c_str(), ROUNDS, std::to_string(i).c_str()) && i != ROUNDS - i - 1) || (std::cout << i << std::endl && false));
 		os << x0.Account ( std::to_string(i).c_str() );
 		os . str ( "" );
 		os << x0.Account ( std::to_string(ROUNDS - i - 1).c_str() );
 		os . str ( "" );
 	}

 	std::cout << "T1" << std::endl;
 	CBank x1 = x0 ;
 	for (size_t i = 0; i < ROUNDS; i++) {
 		assert((x1.Account(std::to_string(i).c_str()).Balance() == 0) || (std::cout << i << " " <<  x1.Account(std::to_string(i).c_str()).Balance() << std::endl && false));
 		
 	}
 	std::cout << "Copy T2" << std::endl;
 	
 	for (size_t i = 0; i < ROUNDS / 2; i++) {
 		assert((x1.Transaction(std::to_string(i).c_str(), std::to_string(ROUNDS - i - 1).c_str(), ROUNDS, std::to_string(i).c_str()) && i != ROUNDS - i - 1) || (std::cout << i << std::endl && false));
 		assert(x1.TrimAccount(std::to_string(i).c_str()));
 	}
 	std::cout << "Trim" << std::endl;
 	for (size_t i = 0; i < ROUNDS / 2; i++) {
 		if (i < ROUNDS / 2) {
 			assert((x1.Account(std::to_string(i).c_str()).Balance() == (int)-ROUNDS) || (std::cout << i << " " <<  x1.Account(std::to_string(i).c_str()).Balance() << std::endl && false));
 		} else {
 			assert((x1.Account(std::to_string(i).c_str()).Balance() == (int)ROUNDS) || (std::cout << i << " " <<  x1.Account(std::to_string(i).c_str()).Balance() << std::endl && false));
 		}
 	}
 	std::cout << "Done" << std::endl;