#ifndef __cvat__ 
#define __cvat__

#include "Invoice.h"
#include "CSortOpt.h"
#include "Date.h"

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <deque>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <memory>

using namespace std;

class CCompany
{
public:
							CCompany						( const string on, 
															const string cn ) 
		:originalName (on),
		cleanName (cn)
	{}
	bool 					insert 							( const CInvoice & src )
	{
		if ( search(name) == true ){
			m_setInvoice . insert ( src );
			return true;
		} else {
			return false;
		}
	}
	bool 					erase							 ( const CInvoice & src )
	{
		// overit ci sa tu uz nachadza, 
		if ( search(name) == true ){
			m_setInvoice . erase ( src );
			return true;
		} else {
			return false;
		}
		// nasledne erase()
	}

	bool 					search 							( const string input ){
		auto got = m_setInvoice.find (input);
		if ( got == m_setInvoice.end() )
			return false;
		else
			return true;
		}

private:
	string originalName, cleanName;
	// mapa faktur podla klucu origianlneho mena (tolower), storuje celu fakturu
	unordered_set<CInvoice, Hasher> m_setInvoice;


	struct 					Hasher
	{
		size_t 		operator()		( const CInvoice & k ) const
		{
			return  ( hash<int>()(k . Date . Year())
					^ hash<int>()(k . Date . Month())
					^ hash<int>()(k . Date . Day())
					^ hash<string>()(k . Buyer()) 
					^ hash<string>()(k . Seller()) 
					^ hash<int>()(k . Amount()) 
					^ hash<double>()(k . VAT()) 
					)
		}
	};

};



class CVATRegister
{
  public:
                             CVATRegister                  ( void ) {}
    bool                     RegisterCompany               ( const string    & n )
    {
    	auto name = to_lower(n);

    	auto reduced = reduce ( name );

    	// vyskytuje sa uz company?
    	if ( search (name) == true ){
    		m_mapCompany . insert ( {name, make_pair(CCompany(),CCompany())} )
    		return true;
    	} else {
    		return false;
    	}
    }
    bool                     AddIssued                     ( const CInvoice  & x )
    {
    	if ( x . Buyer() == x . Seller() ) return false;

    	if ( search (x . Buyer()) == true && search (x . Seller()) == true ){

    		m_mapCompany . insert ( {x . Buyer(), pair::first( CCompany (x . Buyer()) )} )

    		return true;
    	} else {
    		return false;
    	}
    }
    bool                     AddAccepted                   ( const CInvoice  & x )
    {
    	if ( x . Buyer() == x . Seller() ) return false;

    	if ( search (x . Buyer()) == true && search (x . Seller()) == true ){

    		m_mapCompany . insert ( {x . Buyer(), pair::second( CCompany (x . Buyer()) )} )

    		return true;
    	} else {
    		return false;
    	}
    }
    bool                     DelIssued                     ( const CInvoice  & x )
    {
    	if ( x . Buyer() == x . Seller() ) return false;

    	if ( search (x . Buyer()) == true && search (x . Seller()) == true ){

    		m_mapCompany . erase ( {x . Buyer(), pair::first( CCompany (x . Buyer()) )} )

    		return true;
    	} else {
    		return false;
    	}
    }
    bool                     DelAccepted                   ( const CInvoice  & x )
    {
       	if ( x . Buyer() == x . Seller() ) return false;

    	if ( search (x . Buyer()) == true && search (x . Seller()) == true ){

    		m_mapCompany . erase ( {x . Buyer(), pair::second( CCompany (x . Buyer()) )} )

    		return true;
    	} else {
    		return false;
    	}
    }
    list<CInvoice>           Unmatched                     ( const string    & company,
                                                             const CSortOpt  & sortBy ) const
    {}
  private:

  	// mapa firiem, klucom je UPRAVENE meno firmy, storuje jeden par firiem a sice Issued a Accepted
  	// ??? nemusim robit dve mapy, mam rozdelene vsetky faktury uz podla toho kto kupuje a kto predava.
  	unordered_map<string, pair<CCompany,CCompany> > m_mapCompany;

  	bool 					search 							( const string input ){
    	auto search = m_mapCompany . find( name );
	    if( search != m_mapCompany . end() ) {
	    	return true;
	    } else {
	        return false;
	    }
	}

  	// riesenie WS
	string 					trim							(const std::string& str,
	                 										const std::string& whitespace = " ")
	{
	    const auto strBegin = str.find_first_not_of(whitespace);
	    if (strBegin == std::string::npos)
	        return ""; // no content

	    const auto strEnd = str.find_last_not_of(whitespace);
	    const auto strRange = strEnd - strBegin + 1;

	    return str.substr(strBegin, strRange);
	}

	string 					reduced							(const std::string& str,
											                const std::string& fill = " ",
											                const std::string& whitespace = " ")
	{
	    // trim first
	    auto result = trim(str, whitespace);

	    // replace sub ranges
	    auto beginSpace = result.find_first_of(whitespace);
	    while (beginSpace != std::string::npos)
	    {
	        const auto endSpace = result.find_first_not_of(whitespace, beginSpace);
	        const auto range = endSpace - beginSpace;

	        result.replace(beginSpace, range, fill);

	        const auto newStart = beginSpace + fill.length();
	        beginSpace = result.find_first_of(whitespace, newStart);
	    }

	    return result;
	}

};

#endif