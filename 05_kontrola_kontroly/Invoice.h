#ifndef __invoice__ 
#define __invoice__

#include "Date.h"

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <deque>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <memory>
using namespace std;

class CInvoice
{
public:
                             CInvoice                      ( const CDate     & date,
                                                             const string    & seller,
                                                             const string    & buyer,
                                                             unsigned int      amount,
                                                             double            VAT )
                            :m_date ( date ),
                            m_seller ( seller ),
                            m_buyer ( buyer ),
                            m_amount ( amount ),
                            m_VAT ( VAT )
    {}
    CDate                    Date                          ( void ) const
    {
    	return m_date;
    }
    string                   Buyer                         ( void ) const
    {
    	return m_buyer;
    }
    string                   Seller                        ( void ) const
    {
    	return m_seller;
    }
    int                      Amount                        ( void ) const
    {
    	return m_amount;
    }
    double                   VAT                           ( void ) const
    {
    	return m_VAT;
    }
private:
    const CDate     		& m_date;
	string    		& m_seller;
	string    		& m_buyer;
	unsigned int      m_amount;
	double            m_VAT;
};

#endif