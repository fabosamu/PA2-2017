#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <deque>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <memory>
using namespace std;

class CDate
{
  public:
    //---------------------------------------------------------------------------------------------
                CDate                         ( int               y,
                                int               m,
                                int               d )
                : m_Year ( y ),
                m_Month ( m ),
                m_Day ( d )
    {
    }
    //---------------------------------------------------------------------------------------------
    int                      Compare                       ( const CDate     & x ) const
    {
      if ( m_Year != x . m_Year )
        return m_Year - x . m_Year;
      if ( m_Month != x . m_Month )
        return m_Month - x . m_Month;
      return m_Day - x . m_Day;
    }
    //---------------------------------------------------------------------------------------------
    int                      Year                          ( void ) const 
    {
      return m_Year;
    }
    //---------------------------------------------------------------------------------------------
    int                      Month                         ( void ) const 
    {
      return m_Month;
    }
    //---------------------------------------------------------------------------------------------
    int                      Day                           ( void ) const 
    {
      return m_Day;
    }
    //---------------------------------------------------------------------------------------------
    friend ostream         & operator <<                   ( ostream         & os,
                                 const CDate     & x )
    {
      char oldFill = os . fill ();
      return os << setfill ( '0' ) << setw ( 4 ) << x . m_Year << "-" 
                     << setw ( 2 ) << (int) x . m_Month << "-" 
                     << setw ( 2 ) << (int) x . m_Day 
                << setfill ( oldFill );
    }
    //---------------------------------------------------------------------------------------------
  private:
    int16_t                  m_Year;
    int8_t                   m_Month;
    int8_t                   m_Day;
};
#endif /* __PROGTEST__ */


int kitty = 0;







class CInvoice
{
public:
                CInvoice                      ( const CDate     & date,
                                const string    & seller,
                                const string    & buyer,
                                unsigned int      amount,
                                double            VAT )
          :m_date ( date ), m_seller ( seller ), m_buyer ( buyer ), m_amount ( amount ), m_VAT ( VAT )
    {
      m_ord = ++kitty;
    }
    CDate                    Date                          ( void ) const
    {
      return m_date;
    }
    string                   Seller                        ( void ) const
    {
      return m_seller;
    }
    string                   Buyer                         ( void ) const
    {
      return m_buyer;
    }
    int                      Amount                        ( void ) const
    {
      return m_amount;
    }
    double                   VAT                           ( void ) const
    {
      return m_VAT;
    }
    void           setBuyer           (const string & x)
    {
      m_buyer = x;
    }
    void           setSeller            (const string & x)
    {
      m_seller = x;
    }
      
    bool                     operator ==          (const CInvoice & src) const
    {
      return ( Date() . Year() == src . Date() . Year()
            && Date() . Month() == src . Date() . Month()
            && Date() . Day() == src . Date() . Day()
            && Seller() == src . Seller()
            && Buyer() == src . Buyer()
            && Amount() == src . Amount()
            && VAT() == src . VAT()               );
    }

    friend ostream &         operator <<          (ostream & os, const CInvoice & x)
    {
      return os << "Date: " << x.Date().Day() << "." << x.Date().Month() << "." <<  x.Date().Year() << " ; >"
            << x.Seller() << "< >"
            << x.Buyer() << "< "
            << x.Amount() << " "
            << x.VAT() << " "
            << x.m_ord << endl;
    }

    int             m_ord;
private:
    CDate         m_date;
    string              m_seller;
    string              m_buyer;
    unsigned int        m_amount;
    double              m_VAT;
};










class CSortOpt
{
  public:
    static const int      BY_DATE                       = 0;
    static const int      BY_SELLER                     = 2;
    static const int      BY_BUYER                      = 1;
    static const int      BY_AMOUNT                     = 3;
    static const int      BY_VAT                        = 4;
                  CSortOpt                      ( void )
    {
    }
    CSortOpt                  & AddKey                      ( int               key,
                                  bool              ascending = true )
    {
      m_keys . push_back(make_pair(key, ascending));
      return *this;
    }
    bool                  operator ()           ( const CInvoice  & a,
                                                                  const CInvoice  & b ) const
    {
      for (auto x : m_keys){
        string aa1 = a . Seller();
        string bb1 = b . Seller();
        transform( aa1.begin(), aa1.end(), aa1.begin(), ::tolower) ;
        transform( bb1.begin(), bb1.end(), bb1.begin(), ::tolower );
        string aa2 = a . Buyer();
        string bb2 = b . Buyer();
        transform( aa2.begin(), aa2.end(), aa2.begin(), ::tolower) ;
        transform( bb2.begin(), bb2.end(), bb2.begin(), ::tolower );
// cout << x.first;
        switch (x . first)
        {
          case BY_DATE:
          {
            if ( (a.Date().Compare(b.Date()) != b.Date().Compare(a.Date())) ){
              return ( (a.Date().Compare(b.Date()) > b.Date().Compare(a.Date())) ^ x . second ) ;
            }
            break;
          }
          case BY_SELLER:
          {           
            if ( aa1 != bb1 ){
              return ( ( aa1 > bb1 ) ^ x.second );
            }
            break;
          }
          case BY_BUYER:
          {
// cout << aa2 << " "<< bb2 << endl;
            if ( ( aa2 != bb2 ) ){
              return (  ( aa2 > bb2 ) ^ x . second );
            }
            break;
          }
          case BY_AMOUNT:
          {
            if ( (a . Amount() != b . Amount() ) ){
              return ( (a . Amount() > b . Amount() ) ^ x . second );
            }
            break;
          }
          case BY_VAT:
          {
            if ( (a . VAT() != b . VAT() ) ){
              return ( (a . VAT() > b . VAT() ) ^ x . second );
            }
            break;
          }
        }
      }
      // von z cykulu
      return ( a . m_ord < b . m_ord );
    }
  private:
    vector<pair<int,bool> > m_keys;
};


  struct              Hasher
  {
    size_t    operator()    ( const CInvoice & k ) const
    {
      return  ( hash<int>()(k . Date() . Year())
              ^ hash<int>()(k . Date() . Month())
              ^ hash<int>()(k . Date() . Day())
              ^ hash<string>()(k . Seller()) 
              ^ hash<string>()(k . Buyer()) 
              ^ hash<int>()(k . Amount()) 
              ^ hash<double>()(k . VAT())    );
    }
  };







class CCompany
{
public:
                  CCompany                      ( const string & on, 
                                  const string & cn )
    :m_originalName (on),
    m_cleanName (cn) {}
    void            insertIssued                  ( const CInvoice & x )
    {
      if ( m_setIssued.find ( x ) == m_setIssued.end() ){
          m_setIssued . insert( x );
        }
      else
        throw("faktura sa tu uz nachadza CCompany::insertIssued()"); // nenasiel som takuto fakturu
    }
    void            eraseIssued                   ( const CInvoice & x )
    {
      if ( m_setIssued.find ( x ) != m_setIssued.end() ){
          m_setIssued . erase( x );
        }
      else
        throw("nenasiel som fakturu na vymazanie, CCompany::eraseIssued()"); // nenasiel som takuto fakturu
    }

    void            insertAccepted                  ( const CInvoice & x )
    {
      if ( m_setAccepted.find ( x ) == m_setAccepted.end() ){
          m_setAccepted . insert( x );
        }
      else
        throw("faktura sa tu uz nachadza CCompany::insertAccepted()"); // nenasiel som takuto fakturu
    }
    void            eraseAccepted                   ( const CInvoice & x )
    {
      if ( m_setAccepted.find ( x ) != m_setAccepted.end() ){
          m_setAccepted . erase( x );
        }
      else
        throw("nenasiel som fakturu na vymazanie, CCompany::eraseAccepted()"); // nenasiel som takuto fakturu
    }

    vector<CInvoice>      intersection          ( void ) const
    {
      std::vector<CInvoice> v;
      for (auto it : m_setIssued){        
        if ( m_setAccepted . find ( it ) == m_setAccepted . end() ){
          v . push_back(it);
        }
      }

      return v;
    }

    bool            operator ==           (const CCompany & src ) const
    {
      return ( *this == src );
    }
    pair<string,string>     getNames            (void) const
    {
      return make_pair(m_originalName, m_cleanName);
    }
private:

  string              m_originalName;
  string              m_cleanName;

  unordered_set <CInvoice, Hasher> m_setIssued;
  unordered_set <CInvoice, Hasher> m_setAccepted;
};






class CVATRegister
{
  public:
                 CVATRegister                  ( void ) = default;
    bool                     RegisterCompany               ( const string    & name )
    {
      string reduced = reduce(name);
      if ( m_mapCompany . find(reduced) == m_mapCompany . end() ){
        m_mapCompany . insert ( {reduced, CCompany(name, reduced)} );
        return true;
      } else {
        return false;
      }
    }
    bool                     AddIssued                     ( const CInvoice  & src )
    {
      if ( reduce(src . Buyer()) == reduce(src . Seller()) ) return false;
      try {
        CInvoice x = CInvoice(src . Date(),
          m_mapCompany . at(reduce(src . Seller())) . getNames() . first ,
          m_mapCompany . at(reduce(src . Buyer())) . getNames() . first ,
          src . Amount(),
          src . VAT()
         );
        m_mapCompany . at( reduce(x . Seller()) ) . insertIssued (x);
        m_mapCompany . at( reduce(x . Buyer()) ) . insertIssued (x);
        return true;
      } catch (const char * what_arg){
        cout << what_arg << endl;
        return false;
      } catch (exception & e){
        return false;
      }
    }
    bool                     AddAccepted                   ( const CInvoice  & src )
    {
      if ( reduce(src . Buyer()) == reduce(src . Seller()) ) return false;
      try {
        CInvoice x = CInvoice(src . Date(),
          m_mapCompany . at(reduce(src . Seller())) . getNames() . first ,
          m_mapCompany . at(reduce(src . Buyer())) . getNames() . first ,
          src . Amount(),
          src . VAT()
         );
        m_mapCompany . at( reduce( (x . Seller()) ) )  . insertAccepted (x);
        m_mapCompany . at( reduce( (x . Buyer()) ) )  . insertAccepted (x);
        return true;
      } catch (const char * what_arg){
        cout << what_arg << endl;
        return false;
      } catch (exception & e){
        return false;
      }
    }
    bool                     DelIssued                     ( const CInvoice  & x )
    {
      if ( m_mapCompany.find( reduce(x . Seller()) ) == m_mapCompany.end() ) return false;
      if ( m_mapCompany.find( reduce(x . Buyer()) ) == m_mapCompany.end() ) return false;
cout << "BP" << endl;
      // try {
        m_mapCompany . at(reduce(x . Seller())) . eraseIssued (x);
        m_mapCompany . at(reduce(x . Buyer())) . eraseIssued (x);
        return true;
      // } catch (const char * what_arg){
      //  cout << what_arg << endl;
      //  return false;
      // } catch (exception & e){
      //  return false;
      // }
    }
    bool                     DelAccepted                   ( const CInvoice  & x )
    {
      if ( m_mapCompany.find( reduce(x . Seller()) ) == m_mapCompany.end() ) return false;
      if ( m_mapCompany.find( reduce(x . Buyer()) ) == m_mapCompany.end() ) return false;
      // try {
        m_mapCompany . at(reduce(x . Seller())) . eraseAccepted (x);
        m_mapCompany . at(reduce(x . Buyer())). eraseAccepted (x);
        return true;
      // } catch (const char * what_arg){
      //  cout << what_arg << endl;
      //  return false;
      // } catch (exception & e){
      //  return false;
      // }
    }
    list<CInvoice>           Unmatched                     ( const string    & company,
                                 const CSortOpt  & sortBy ) const
    {
      try {
        vector<CInvoice> v = m_mapCompany . at( reduce(company) ) . intersection();
        sort(v.begin(), v.end(), sortBy);
        list<CInvoice> list;
        copy( v.begin(), v.end(), back_inserter( list ) );
        return list;
      } catch ( exception & e ){
        cout << e . what();
        return list<CInvoice>();
      }
    }
  private:
    unordered_map <string, CCompany > m_mapCompany;

    string          reduce              (const string & temp) const
    {
      string tempName;  
      tempName.clear();
        unique_copy ( temp.begin(), temp.end(), back_insert_iterator<string>(tempName),
                                         [](char a,char b){ return isspace(a) && isspace(b);} );

        tempName.erase ( tempName.begin(), std::find_if(tempName.begin(), tempName.end(), std::bind1st(std::not_equal_to<char>(), ' ')) );

        tempName.erase ( std::find_if(tempName.rbegin(), tempName.rend(), std::bind1st(std::not_equal_to<char>(), ' ') ).base(), tempName.end());
        std::transform ( tempName.begin(), tempName.end(), tempName.begin(), ::tolower );
        return tempName;
    }
};

#ifndef __PROGTEST__
bool equalLists ( const list<CInvoice> & a, const list<CInvoice> & b )
{
cout << a . size() << " velkosti " << b . size() << endl;
  return equal( a.begin(), a.end(), b.begin() );
  // bullshit
}

int main ( void )
{
  CVATRegister r;
  assert ( r . RegisterCompany ( "first Company" ) );
  assert ( r . RegisterCompany ( "Second     Company" ) );
  assert ( r . RegisterCompany ( "ThirdCompany, Ltd." ) );
  assert ( r . RegisterCompany ( "Third Company, Ltd." ) );
  assert ( !r . RegisterCompany ( "Third Company, Ltd." ) );
  assert ( !r . RegisterCompany ( " Third  Company,  Ltd.  " ) );
  assert ( r . AddIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Company ", 100, 20 ) ) );
  assert ( r . AddIssued ( CInvoice ( CDate ( 2000, 1, 2 ), "FirSt Company", "Second Company ", 200, 30 ) ) );
  assert ( r . AddIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Company ", 100, 30 ) ) );
  assert ( r . AddIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Company ", 300, 30 ) ) );
  assert ( r . AddIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", " Third  Company,  Ltd.   ", 200, 30 ) ) );
  assert ( r . AddIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "Second Company ", "First Company", 300, 30 ) ) );
  assert ( r . AddIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "Third  Company,  Ltd.", "  Second    COMPANY ", 400, 34 ) ) );
  assert ( !r . AddIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Company ", 300, 30 ) ) );
  assert ( !r . AddIssued ( CInvoice ( CDate ( 2000, 1, 4 ), "First Company", "First   Company", 200, 30 ) ) );
  assert ( !r . AddIssued ( CInvoice ( CDate ( 2000, 1, 4 ), "Another Company", "First   Company", 200, 30 ) ) );

// list <CInvoice> test = r . Unmatched ( "First Company", CSortOpt () . AddKey ( CSortOpt::BY_SELLER, true ) . AddKey ( CSortOpt::BY_BUYER, false ) . AddKey ( CSortOpt::BY_DATE, false ) );
// if (test.empty()) cout << "si kokot" << endl;
// for (CInvoice n : test) {
//     std::cout << n << '\n';
// }
  assert ( equalLists ( r . Unmatched ( "First Company", CSortOpt () . AddKey ( CSortOpt::BY_SELLER, true ) . AddKey ( CSortOpt::BY_BUYER, false ) . AddKey ( CSortOpt::BY_DATE, false ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Third Company, Ltd.", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 )
           } ) );
//  cout << endl;
//  test = r . Unmatched ( "First Company", CSortOpt () . AddKey ( CSortOpt::BY_DATE, true ) . AddKey ( CSortOpt::BY_SELLER, true ) . AddKey ( CSortOpt::BY_BUYER, true ) );
// if (test.empty()) cout << "si kokot" << endl;
// for (CInvoice n : test) {
//     std::cout << n << '\n';
// }
  assert ( equalLists ( r . Unmatched ( "First Company", CSortOpt () . AddKey ( CSortOpt::BY_DATE, true ) . AddKey ( CSortOpt::BY_SELLER, true ) . AddKey ( CSortOpt::BY_BUYER, true ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Third Company, Ltd.", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 )
           } ) );
  assert ( equalLists ( r . Unmatched ( "First Company", CSortOpt () . AddKey ( CSortOpt::BY_VAT, true ) . AddKey ( CSortOpt::BY_AMOUNT, true ) . AddKey ( CSortOpt::BY_DATE, true ) . AddKey ( CSortOpt::BY_SELLER, true ) . AddKey ( CSortOpt::BY_BUYER, true ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Third Company, Ltd.", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 )
           } ) );
  assert ( equalLists ( r . Unmatched ( "First Company", CSortOpt () ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Third Company, Ltd.", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 )
           } ) );
  assert ( equalLists ( r . Unmatched ( "second company", CSortOpt () . AddKey ( CSortOpt::BY_DATE, false ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Third Company, Ltd.", "Second     Company", 400, 34.000000 )
           } ) );
  assert ( equalLists ( r . Unmatched ( "last company", CSortOpt () . AddKey ( CSortOpt::BY_VAT, true ) ),
           list<CInvoice>
           {
           } ) );
  assert ( r . AddAccepted ( CInvoice ( CDate ( 2000, 1, 2 ), "First Company", "Second Company ", 200, 30 ) ) );
  assert ( r . AddAccepted ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", " Third  Company,  Ltd.   ", 200, 30 ) ) );
  assert ( r . AddAccepted ( CInvoice ( CDate ( 2000, 1, 1 ), "Second company ", "First Company", 300, 32 ) ) );
  assert ( equalLists ( r . Unmatched ( "First Company", CSortOpt () . AddKey ( CSortOpt::BY_SELLER, true ) . AddKey ( CSortOpt::BY_BUYER, true ) . AddKey ( CSortOpt::BY_DATE, true ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 32.000000 )
           } ) );
  assert ( !r . DelIssued ( CInvoice ( CDate ( 2001, 1, 1 ), "First Company", "Second Company ", 200, 30 ) ) );
  assert ( !r . DelIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "A First Company", "Second Company ", 200, 30 ) ) );
  assert ( !r . DelIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Hand", 200, 30 ) ) );
  assert ( !r . DelIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Company", 1200, 30 ) ) );
  assert ( !r . DelIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Company", 200, 130 ) ) );
cout << "BPa" << endl;
  assert ( r . DelIssued ( CInvoice ( CDate ( 2000, 1, 2 ), "First Company", "Second Company", 200, 30 ) ) );
  assert ( equalLists ( r . Unmatched ( "First Company", CSortOpt () . AddKey ( CSortOpt::BY_SELLER, true ) . AddKey ( CSortOpt::BY_BUYER, true ) . AddKey ( CSortOpt::BY_DATE, true ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 32.000000 )
           } ) );
  assert ( r . DelAccepted ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", " Third  Company,  Ltd.   ", 200, 30 ) ) );
  assert ( equalLists ( r . Unmatched ( "First Company", CSortOpt () . AddKey ( CSortOpt::BY_SELLER, true ) . AddKey ( CSortOpt::BY_BUYER, true ) . AddKey ( CSortOpt::BY_DATE, true ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Third Company, Ltd.", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 32.000000 )
           } ) );
  assert ( r . DelIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", " Third  Company,  Ltd.   ", 200, 30 ) ) );
  assert ( equalLists ( r . Unmatched ( "First Company", CSortOpt () . AddKey ( CSortOpt::BY_SELLER, true ) . AddKey ( CSortOpt::BY_BUYER, true ) . AddKey ( CSortOpt::BY_DATE, true ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 32.000000 )
           } ) );
  return 0;
}
#endif /* __PROGTEST__ */
