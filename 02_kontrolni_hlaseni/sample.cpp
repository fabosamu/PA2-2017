#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <cassert>
#include <iostream> 
#include <iomanip> 
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <memory>

 #include "hashtable.h"

using namespace std;
#endif /* __PROGTEST__ */




class CVATRegister
{
  public:
                  CVATRegister   ( void ){}
                  ~CVATRegister  ( void ){}


    bool          NewCompany     ( const string    & name,
                                   const string    & addr,
                                   const string    & taxID ){
      // mam dvojprvok pre meno a adresu oddeleny oddelovacom.
      // porovnam ci sa prvok uz v databazi nachadza
      // cout << "**********new company" << endl;


      try {

        if ( companies[tableID.get(taxID)] . empty == false ){
          return false;
        }

      } catch (const char * msg){
        // cout << msg << endl;
      }

      try {

        if ( companies[tableNA.get(lowCat(name, addr))] . empty == false ){
          return false;
        }

      } catch (const char * msg){
        // cout << endl;
      }

      Company c;
      c.nameAddr = lowCat(name, addr);
      c.taxID = taxID;
      c.empty = false;
      companies.push_back(c);

      tableNA . add(lowCat(name, addr), companies.size() - 1 ); 
      tableID . add(taxID, companies.size() - 1 ); 
      // tableID . print();
      // tableNA . print();
      
      if (taxID == "abcdef") {
        for (auto c : companies) {
          // std::cout << c.taxID << " " << c.nameAddr << " " << c.empty << std::endl;;
        }
        // std::cout << 
      }

      return true;

    }


    bool          CancelCompany  ( const string    & name,
                                   const string    & addr ){
      // nameAddr = lowCat(name, addr);
      // cout << "**********cancel company" << endl;

      if ( tableNA.isIn( lowCat(name, addr) ) ){

        if ( companies[tableNA.get(lowCat(name, addr))] . empty == false ){
          companies[tableNA.get(lowCat(name, addr))] . empty = true;
          return true;

        } else {

          // cout << " name address " << tableNA.get(lowCat(name, addr)) << " | " << endl;
          return false;
        }

      } else {
        // cout << "nie je tu company" << endl;
        return false;
      }

    }
    bool          CancelCompany  ( const string    & taxID ){
      // cout << "**********cancel company" << endl;
      if ( tableID.isIn(taxID) ){
        if ( companies[tableID.get(taxID)] . empty == false ){
          companies[tableID.get(taxID)] . empty = true; 
          return true;

        } else {
          return false;
        }

      } else {
        // cout << "nie je tu company" << endl;
        return false;
      }


    }
    bool          Invoice        ( const string    & taxID,
                                   unsigned int      amount ){
      try{
        companies[tableID.get(taxID)] . amount += amount;
      } catch (const char * msg) {
        // cout << msg << endl;
        return false;
      }
          invoices.push_back(amount);
      return true;
    }
    bool          Invoice        ( const string    & name,
                                   const string    & addr,
                                   unsigned int      amount ){
      try{
        companies[tableNA.get( lowCat(name, addr) )] . amount += amount;
      } catch (const char * msg) {
        // cout << msg << endl;
        return false;
      }
        invoices.push_back(amount);
      return true;
    }
    bool          Audit          ( const string    & name,
                                   const string    & addr,
                                   unsigned int    & sumIncome ) const{
      try{
        sumIncome = companies[tableNA.get( lowCat(name, addr) )] . amount;
      } catch (const char * msg) {
        // cout << msg << endl;
        return false;
      }
      return true;
    }
    bool          Audit          ( const string    & taxID,
                                   unsigned int    & sumIncome ) const{
      try{
        sumIncome = companies[tableID.get( taxID )] . amount;
      } catch (const char * msg) {
        // cout << msg << endl;
        return false;
      }
      return true;
    }
    unsigned int  MedianInvoice  ( void ) const{
      // cout << "**********median" << endl;

      std::vector<uint> tmp;

      for (uint i = 0; i < invoices.size(); ++i)
      {
        // cout << invoices[i] << " ";
        tmp.push_back(invoices[i]);
      }
      // cout << endl;
      
      std::sort(tmp.begin(), tmp.end()); 

      // for (uint i = 0; i < tmp.size(); ++i)
      // {
      //   cout << tmp[i] << endl;
      // }
      // cout << "end\n" << endl;

      if ( tmp.size() == 0 ) return 0;
      if ( tmp.size() % 2 ){
        // cout << tmp[(tmp.size()/2)] << endl;
        return tmp[(tmp.size()/2)];
      } else {
        // cout << tmp[(tmp.size()/2)] << endl;
        return tmp[(tmp.size()/2)];
      }
    }
    
  private:
    HashTable tableID, tableNA;
    struct Company
    {
      std::string nameAddr;
      std::string taxID;
      uint amount;
      bool empty;
      Company() {empty = true;amount = 0;}
    };
    std::vector<Company> companies;
    std::vector<uint> invoices;

    std::string lowCat(const std::string name, const std::string addr) const {
      std::string n = name, a = addr;
      std::transform(n.begin(), n.end(), n.begin(), ::tolower);
      std::transform(a.begin(), a.end(), a.begin(), ::tolower);
      return n + "=|=" + a;
    }
};

#ifndef __PROGTEST__
int main ( void )
{

  unsigned int sumIncome;

  CVATRegister b1;
  assert ( b1 . NewCompany ( "ACME", "Kolejni", "666/666/666" ) );
  assert ( b1 . NewCompany ( "ACME", "Thakurova", "666/666" ) );
  assert ( b1 . NewCompany ( "Dummy", "Thakurova", "123456" ) );
  assert ( b1 . Invoice ( "666/666", 2000 ) );
  assert ( b1 . MedianInvoice () == 2000 );
  assert ( b1 . Invoice ( "666/666/666", 3000 ) );
  assert ( b1 . MedianInvoice () == 3000 );
  assert ( b1 . Invoice ( "123456", 4000 ) );
  assert ( b1 . MedianInvoice () == 3000 );
  assert ( b1 . Invoice ( "aCmE", "Kolejni", 5000 ) );
  assert ( b1 . MedianInvoice () == 4000 );
  assert ( b1 . Audit ( "ACME", "Kolejni", sumIncome ) && sumIncome == 8000 );
  assert ( b1 . Audit ( "123456", sumIncome ) && sumIncome == 4000 );
  assert ( b1 . CancelCompany ( "ACME", "KoLeJnI" ) );
  assert ( b1 . MedianInvoice () == 4000 );
  assert ( b1 . CancelCompany ( "666/666" ) );
  assert ( b1 . MedianInvoice () == 4000 );
  assert ( b1 . Invoice ( "123456", 100 ) );
  assert ( b1 . MedianInvoice () == 3000 );
  assert ( b1 . Invoice ( "123456", 300 ) );
  assert ( b1 . MedianInvoice () == 3000 );
  assert ( b1 . Invoice ( "123456", 200 ) );
  assert ( b1 . MedianInvoice () == 2000 );
  assert ( b1 . Invoice ( "123456", 230 ) );
  assert ( b1 . MedianInvoice () == 2000 );
  assert ( b1 . Invoice ( "123456", 830 ) );
  assert ( b1 . MedianInvoice () == 830 );
  assert ( b1 . Invoice ( "123456", 1830 ) );
  assert ( b1 . MedianInvoice () == 1830 );
  assert ( b1 . Invoice ( "123456", 2830 ) );
  assert ( b1 . MedianInvoice () == 1830 );
  assert ( b1 . Invoice ( "123456", 2830 ) );
  assert ( b1 . MedianInvoice () == 2000 );
  assert ( b1 . Invoice ( "123456", 3200 ) );
  assert ( b1 . MedianInvoice () == 2000 );

   // cout << "continue b2" << endl;

  CVATRegister b2;
  assert ( b2 . NewCompany ( "ACME", "Kolejni", "abcdef" ) );
  assert ( b2 . NewCompany ( "Dummy", "Kolejni", "123456" ) );
  assert ( ! b2 . NewCompany ( "AcMe", "kOlEjNi", "1234" ) );
  assert ( b2 . NewCompany ( "Dummy", "Thakurova", "ABCDEF" ) );
  assert ( b2 . MedianInvoice () == 0 );
  assert ( b2 . Invoice ( "ABCDEF", 1000 ) );
  assert ( b2 . MedianInvoice () == 1000 );
  assert ( b2 . Invoice ( "abcdef", 2000 ) );
  assert ( b2 . MedianInvoice () == 2000 );
  assert ( b2 . Invoice ( "aCMe", "kOlEjNi", 3000 ) );
  assert ( b2 . MedianInvoice () == 2000 );
  assert ( ! b2 . Invoice ( "1234567", 100 ) );
  assert ( ! b2 . Invoice ( "ACE", "Kolejni", 100 ) );
  assert ( ! b2 . Invoice ( "ACME", "Thakurova", 100 ) );
  assert ( ! b2 . Audit ( "1234567", sumIncome ) );
  assert ( ! b2 . Audit ( "ACE", "Kolejni", sumIncome ) );
  assert ( ! b2 . Audit ( "ACME", "Thakurova", sumIncome ) );
  assert ( ! b2 . CancelCompany ( "1234567" ) );
  assert ( ! b2 . CancelCompany ( "ACE", "Kolejni" ) );
  assert ( ! b2 . CancelCompany ( "ACME", "Thakurova" ) );
  assert ( b2 . CancelCompany ( "abcdef" ) );
  assert ( b2 . MedianInvoice () == 2000 );
  assert ( ! b2 . CancelCompany ( "abcdef" ) );
  assert ( b2 . NewCompany ( "ACME", "Kolejni", "abcdef" ) );
  assert ( b2 . CancelCompany ( "ACME", "Kolejni" ) );
  assert ( ! b2 . CancelCompany ( "ACME", "Kolejni" ) );
// cout << "done!" << endl;
  return 0;
}
#endif /* __PROGTEST__ */
