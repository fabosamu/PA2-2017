#include <iostream>
#include <string>
#include <vector>

class HashTable
{
private:
    struct Pair
    {
        std::string key;
        int index;
    };
    std::hash<std::string> hash;
    std::vector< std::vector<Pair> > table;
    uint itemCounter;
    
    void rehash(){
        std::vector< std::vector<Pair> > temp (table.size() * 2, std::vector<Pair>());
        for ( uint i = 0 ; i < table.size() ; i++ ){
            for ( uint j = 0 ; j < table[i].size() ; j++ ){
                auto value = hash( table[i][j].key );
                auto modValue = value % temp.size();
                Pair pair = { table[i][j].key, table[i][j].index };
                temp[modValue].push_back(pair);
            }
        }
        table = temp;
    }
    
public:
    HashTable() { itemCounter = 0; table = std::vector< std::vector<Pair> >(8, std::vector<Pair>()); }
    ~HashTable(){}
    bool add(std::string key, int index){
        if ( isIn(key) ) {
            
            get(key) = index;
            
            return true;
            
        } else {
            // progtest overi, ci netreba radsej po tych dvoch tretinach prehashovat.. alebo ked je dlhsi 2.rozmer
            
            if ( itemCounter == table.size() ) rehash();
            
            // std::cout << "adding " << key << " " << index << std::endl;
            
            auto value = hash(key);
            auto modValue = value % table.size();
            Pair pair;
            pair . key = key;
            pair . index = index;
            
            table[modValue].push_back(pair);
            itemCounter++;
        }
        
        return true;
    }
    
    bool isIn(std::string key){
        auto value = hash(key);
        auto modValue = value % table.size();
        
        for ( uint i = 0 ; i < table[modValue].size() ; i++ ){
            if (table[modValue][i].key == key) {
                return true;
            }
        }
        
        // throw ("isIn: cant find in table");
        return false;
    }
    
    int &get(std::string key) {
        auto value = hash(key);
        auto modValue = value % table.size();
        
        for ( uint i = 0 ; i < table[modValue].size() ; i++ ){
            if (table[modValue][i].key == key) {
                return table[modValue][i].index;
            }
        }
        throw " get: check with .isIn()";
    }
    
    int get(std::string key) const {
        auto value = hash(key);
        auto modValue = value % table.size();
        
        for ( uint i = 0 ; i < table[modValue].size() ; i++ ){
            if (table[modValue][i].key == key) {
                return table[modValue][i].index;
            }
        }
        throw " get: check with .isIn()";
    }
    
    void print(){
        cout << "===HASH TABLE===" << endl;
        for (uint j = 0; j < table.size() ; j++){
            std::cout << "hashed key: " << j << ":\t ";
            for ( uint i = 0 ; i < table[j].size() ; i++ ){
                std::cout << table[j][i].key << " ; " << table[j][i].index << " || ";
            }
            std::cout << " ::size: " << table[j].size() << std::endl;
        }
    }
    
};
