# Programming and Algorithmics 2

These are all project I have done in my second semester. Written in C++ (while learning C++ and OOP).  

## Names of the projects and rating by school.

Name          	 		| Rating  |
----------------------- | ------- |
02 Kontrolní hlášení	| 5.5/5  |
03 Polynomy			| 5.5/5  |
04 e-Banking		| 5.0/5  |
05 Výsledková listina	  	| 6.5/5  |
06 Prázdniny se blíží I	| 5.0/5  |

